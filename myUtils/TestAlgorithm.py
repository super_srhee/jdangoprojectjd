from py2neo import Graph
# Create py2neo connection
graph = Graph(host='localhost',auth=('neo4j','123'))

def Test():
    global graph
    n = 1
    same = []
    diff = []
    while n <= 65:
        num = 980 * n
        commonSql = "MATCH (p1:Facebook_users{UserID:'" + str(
            num) + "'}) -[:FRIENDS_WITH]-(p2:Facebook_users)-[:FRIENDS_WITH]-(p3:Facebook_users) where p1<>p3 return p3.UserID as UserID, COUNT(*) AS p_number ORDER BY p_number DESC LIMIT 10 "
        influenceSql = "MATCH (p1:Facebook_users{UserID:'" + str(
            num) + "'})-[:FRIENDS_WITH]- (p2:Facebook_users) -[:FRIENDS_WITH]-(p3:Facebook_users) where p1<>p3 with p2,count(p2) as c1,p1 match (p1) -[:FRIENDS_WITH]- (p2) -[:FRIENDS_WITH]-(p3) where p1<>p3 return p3.UserID as UserID,sum(1/c1) as c order by c desc limit 10"

        commonNodes = graph.run(commonSql).data()
        commonUserIds = []
        influenceUserIds = []
        for node in commonNodes:
            commonUserIds.append(node.get("UserID"))

        influenceNodes = graph.run(influenceSql).data()
        for node in influenceNodes:
            influenceUserIds.append(node.get("UserID"))

        # print(commonUserIds)
        # print(influenceUserIds)
        # compare set
        result = set(commonUserIds) - set(influenceUserIds)
        if result == set():
            same.append(num)
        else:
            diff.append(num)

        n += 1

    print(same)
    print(diff)


    print('over.')


if __name__ == '__main__':
    Test()