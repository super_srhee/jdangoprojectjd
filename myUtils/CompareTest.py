from py2neo import Graph,Node,Relationship
# Create py2neo connection
graph = Graph(host='localhost',auth=('neo4j','123'))
# Create your views here.

def main():
    global graph
    n = 1
    same1 = []
    same2 = []
    commonWeight = 3/8  # Compare common VS Influence weight .InfluenceWeight = 1 - commonWeight
    influenceWeight = 1 - commonWeight
    while n <= 500:
        num = 60 * n

        generalRecommendationSql = "MATCH (p1:Facebook_users{UserID:'" + str(
            num) + "'}) -[:FRIENDS_WITH]-(p2:Facebook_users)-[:FRIENDS_WITH]-(p3:Facebook_users) where p1<>p3 return p3.UserID as UserID, COUNT(*) AS p_number ORDER BY p_number DESC LIMIT 10 "
        influenceSql = "MATCH (p1:Facebook_users{UserID:'" + str(
            num) + "'})-[:FRIENDS_WITH]- (p2:Facebook_users) -[:FRIENDS_WITH]-(p3:Facebook_users) where p1<>p3 with p2,count(p2) as c1,p1 match (p1) -[:FRIENDS_WITH]- (p2) -[:FRIENDS_WITH]-(p3) where p1<>p3 return p3.UserID as UserID,sum(100/c1) as c order by c desc limit 10"

        compareSql = "MATCH (p:Facebook_users{UserID:'" + str(num) + "'}) -[:FRIENDS_WITH]- (p1:Facebook_users) - [:FRIENDS_WITH]-(p3:Facebook_users) where p1 <> p3 with p1,count(p1) as c1,p match (p) -[:FRIENDS_WITH]- (p1) -[:FRIENDS_WITH]-(p2) where p<>p2 return p2.UserID as UserID, count(c1)*"+ str(commonWeight) +" + sum(100/(c1))*"+ str(influenceWeight) + " as c order by c desc limit 10"

        commonUserIds = []
        influenceUserIds = []
        compareUserIds = []

        commonNodes = graph.run(generalRecommendationSql).data()
        for node in commonNodes:
            commonUserIds.append(node.get("UserID"))

        influenceNodes = graph.run(influenceSql).data()
        for node in influenceNodes:
            influenceUserIds.append(node.get("UserID"))

        compareNodes = graph.run(compareSql).data()
        for node in compareNodes:
            compareUserIds.append(node.get("UserID"))

        # print(commonUserIds)
        # print(influenceUserIds)
        # compare set
        result1 = set(commonUserIds) - set(compareUserIds)
        if result1 == set():
            same1.append(num)

        result2 = set(influenceUserIds) - set(compareUserIds)
        if result2 == set():
            same2.append(num)


        n += 1

    print(len(same1))
    print(len(same2))


    print('over.')


if __name__ == '__main__':
    main()