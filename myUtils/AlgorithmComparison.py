from py2neo import Graph

if __name__ == '__main__':
    graph = Graph(host='localhost',auth=('neo4j','123')) # Create py2neo connection
    n = 1
    same = []
    diff = []
    while n <= 65:
        num = 980 * n

        generalRecommendationSql = "MATCH (f1:Facebook_users{UserID:" + str(
            num) + "}) -[:FRIENDS_WITH]-(f2:Facebook_users)-[:FRIENDS_WITH]-(f3:Facebook_users) where f1<>f3 with f3.UserID AS UserID, COUNT(*) AS c ORDER BY c DESC LIMIT 10 return collect(UserID) as us"
        influenceRecommendationSql = "MATCH (f1:Facebook_users{UserID:" + str(
            num) + "})-[r1:FRIENDS_WITH]- (f2:Facebook_users) -[:FRIENDS_WITH]-(f3:Facebook_users) where f1<>f3 with f2,count(f2) as c1,f1 match (f1) -[:FRIENDS_WITH]- (f2) -[r1:FRIENDS_WITH]-(f3) where f1<>f3 with f3.UserID as u,sum(1/c1) as c order by c desc limit 10 return collect(u) as us"

        generalNodes = graph.run(generalRecommendationSql).data()
        generalUserIds = generalNodes[0].get("us")
        influenceNodes = graph.run(influenceRecommendationSql).data()
        influenceUserIds = influenceNodes[0].get("us")

        result = set(generalUserIds) - set(influenceUserIds)
        if result == set():
            same.append(num)
        else:
            diff.append(num)

        n += 1

    print(same)
    print(diff)