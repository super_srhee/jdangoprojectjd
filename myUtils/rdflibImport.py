#coding:utf8
import rdflib,pandas as pd
from py2neo import Graph,Node,Relationship

def count_nine_v2(fname):
    """计算文件里包含多少个数字 '9'，每次读取 8kb
    """
    count = 0
    block_size = 1024 * 10
    with open(fname) as fp:
        while True:
            chunk = fp.read(block_size)
            # 当文件没有更多内容时，read 调用将会返回空字符串 ''
            if not chunk:
                break
            else:
                # list = chunk.split('skos:prefLabel')
                list = chunk.splitlines()
                for item in list:
                    # print(item)
                    if item.find('Internet') !=-1:
                        print(chunk)
            count += chunk.count('9')
    return count

if __name__ == '__main__':
    graph = Graph(host='localhost',auth=('neo4j','123')) # Create py2neo connection

    g2 = rdflib.Graph()
    # subject = g2.parse(r'C:\Users\srhee\Documents\Tencent Files\851356263\FileRecv\商务智能_课程项目_数据集\OpenPermID-bulk-assetClass-20210613_051535\OpenPermID-bulk-assetClass-20210613_051535.ttl', format='turtle')
    # subject = g2.parse(r'C:\Users\srhee\Documents\Tencent Files\851356263\FileRecv\商务智能_课程项目_数据集\OpenPermID-bulk-industry-20210613_051530.ttl', format='turtle')
    # subject = g2.subjects(p, o)
    # for triple in subject:
    #     print(triple)
        # print(triple.subject, triple.predicate, triple.object)

    # formatStr = rdflib.util.guess_format(r'C:\Users\srhee\Documents\Tencent Files\851356263\FileRecv\商务智能_课程项目_数据集\OpenPermID-bulk-person-20210613_060851.ttl')
    # print(formatStr)

    count  = count_nine_v2(r'C:\Users\srhee\Documents\Tencent Files\851356263\FileRecv\商务智能_课程项目_数据集\OpenPermID-bulk-industry-20210613_051530.ttl')
