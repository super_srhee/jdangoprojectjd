from py2neo import Graph,Node,Relationship

if __name__ == '__main__':
    graph = Graph(host='localhost',auth=('neo4j','123')) # Create py2neo connection
    n = 1
    same1 = []
    same2 = []

    while n <= 1000:
        num = 50 * n

        generalRecommendationSql = "MATCH (f1:Facebook_users{UserID:" + str(
            num) + "}) -[:FRIENDS_WITH]-(f2:Facebook_users)-[:FRIENDS_WITH]-(f3:Facebook_users) where f1<>f3 with f3.UserID AS UserID, COUNT(*) AS c ORDER BY c DESC return collect(UserID) as us"
        influenceRecommendationSql = "MATCH (f1:Facebook_users{UserID:" + str(
            num) + "})-[r1:FRIENDS_WITH]- (f2:Facebook_users) -[:FRIENDS_WITH]-(f3:Facebook_users) where f1<>f3 with f2,count(f2) as c1,f1 match (f1) -[:FRIENDS_WITH]- (f2) -[r1:FRIENDS_WITH]-(f3) where f1<>f3 with f3.UserID as u,sum(1/c1) as c order by c desc return collect(u) as us"

        commonUserIds = []
        influenceUserIds = []

        commonNodes = graph.run(generalRecommendationSql).data()
        for node in commonNodes:
            commonUserIds.append(node.get("UserID"))

        influenceNodes = graph.run(influenceRecommendationSql).data()
        for node in influenceNodes:
            influenceUserIds.append(node.get("UserID"))

        # compare set
        result1 = set(commonUserIds) & set(influenceUserIds)
        result2 = set(commonUserIds) | set(influenceUserIds)
        rate = len(result1)/len(result2) *100

        n += 1

    print(str(rate) +"/100")