# 德劳内三角网实现算法，支持各种几何
# 前置： pip3 install shapely,pyshp
import shapefile, geojson,math
from shapely.ops import triangulate
from shapely import wkt
from shapely.geometry import MultiPoint,multipolygon,Polygon,MultiPolygon
import shapely.geometry as geometry

path = r'C:\Users\srhee\Desktop\poly\polygon3.shp'
sf = shapefile.Reader(path)

# polygon = Polygon([(0, 0), (1, 1), (1, 0)])
# print(polygon.area)

shapes = sf.shapes()
# print(shapes)

coord = []
w1 = shapefile.Writer(r'C:\Users\srhee\Desktop\poly\polygonT')
w1.field('name', 'C')

w = shapefile.Writer(r'C:\Users\srhee\Desktop\poly\polygonR')
w.field('name', 'C')

for i in range(len(shapes)):
    shape = sf.shape(i)
    print(shape.shapeType)
    print(shape.shapeTypeName)
    print(shape.wkt)
    # points = MultiPoint(shape.points)
    print(shape.points)
    # g11 = wkt.loads(j.wkt)

    points = Polygon(shape.points)
    # g12 = geojson.Feature(geometry=wkt.loads(points.wkt), properties={})
    g12 = geojson.Feature(geometry=shape, properties={})
    print(g12)
    print(111111111)
    print(g12.geometry.coordinates)
    # print(shape.wkt)
    w1.poly(g12.geometry.coordinates)
    w1.record('polygon')

    # points2 = Polygon(g12.geometry.coordinates)

    triangles = triangulate(points, tolerance=0.00001,edges=False)
    # print(triangles)

    for j in triangles:
        g1 = wkt.loads(j.wkt)
        g2 = geojson.Feature(geometry=g1, properties={})
        # print(g2.geometry.coordinates)
        w.poly(g2.geometry.coordinates)
        w.record('polygon')
        # coord.append(g2.geometry.coordinates[0])

    # fnew = open(r'delaunay.txt','a',encoding='utf-8')
    # for index,t in enumerate(triangles):
    #
    #     fnew.write(str(index)+'\t'+str(t.wkt)+'\n')
    #     print(t.wkt)


w.close()
w1.close()

# mutlipoint = MultiPoint([114,23])
# print(mutlipoint)

# f.close()
# fnew.close()