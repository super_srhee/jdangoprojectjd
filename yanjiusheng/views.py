from django.shortcuts import render
import json,re,random,io,sys,requests,time

from django.views import View
from django.http import JsonResponse,QueryDict,HttpResponse
from django.views.decorators.csrf import csrf_exempt

from py2neo import Graph,Node,Relationship
graph = Graph(host='localhost',auth=('neo4j','123'))

# Create your views here.

##
# LOAD CSV with headers from 'file:///yanjiusheng/论文管理数据信息.csv' as line
# merge (lunwen:LunWen{序号:line.`序号`, 题目:line.`题目`,作者:line.`作者`,来源:line.`来源`,发表时间:line.`发表时间`,论文级别:line.`论文级别`,研究方向:line.`研究方向`,关键词:line.`关键词`})
# merge(yanjiufangxiang:YanJiuFangXiang{研究方向:line.`研究方向`})
# merge (yanjiufangxiang)-[:Type]->(lunwen)
#
# LOAD CSV with headers from 'file:///yanjiusheng/科技竞赛管理数据信息.csv' as line
# merge (kejijingsai:KeJiJingSai{序号:line.`序号`, 名称:line.`名称`,类型:line.`类型`,级别:line.`级别`,涉及学科知识:line.`涉及学科知识`,成果展示:line.`成果展示`})
# merge(kejijingsaitype:KeJiJingSaiType{类型:line.`类型`})
# merge (kejijingsaitype)-[:KeJiJingSaiType]->(kejijingsai)
# #

@csrf_exempt
def lunwen_findTypes(request):
    global graph
    sql = "MATCH (n:YanJiuFangXiang) RETURN n as type"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def lunwen_findByType(request):
    global graph
    type = eval(request.body.decode()).get('type')
    sql = "MATCH (n:YanJiuFangXiang) -[]->(lunwen)  where n.`研究方向` = '"+ type +"' RETURN n as type, lunwen"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def lunwen_findByParams(request):
    global graph
    # sql = eval(request.body.decode()).get('sql')
    typePre = eval(request.body.decode()).get('typePre')
    typeNext = eval(request.body.decode()).get('typeNext')
    inputNext = eval(request.body.decode()).get('inputNext')

    if typePre == '' or typeNext == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            sql = "MATCH (type:YanJiuFangXiang) -[]->( lunwen:LunWen) where type.`研究方向` = '"+typePre+"' and lunwen.`"+typeNext+"` =~'.*" + inputNext + ".*' Return type, lunwen"
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})

# 科技竞赛
@csrf_exempt
def kejijingsai_findTypes(request):
    global graph
    sql = "MATCH (n:KeJiJingSaiType) RETURN n as type"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def kejijingsai_findByType(request):
    global graph
    type = eval(request.body.decode()).get('type')
    sql = "MATCH (n:KeJiJingSaiType) -[]-> (kejijingsai)  where n.`类型` = '"+ type +"' RETURN n as type, kejijingsai"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def kejijingsai_findByParams(request):
    global graph
    # sql = eval(request.body.decode()).get('sql')
    typePre = eval(request.body.decode()).get('typePre')
    typeNext = eval(request.body.decode()).get('typeNext')
    inputNext = eval(request.body.decode()).get('inputNext')

    if typePre == '' or typeNext == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            sql = "MATCH (type:KeJiJingSaiType) -[]->(kejijingsai:KeJiJingSai) where type.`类型` = '"+typePre+"' and kejijingsai.`"+typeNext+"` =~'.*" + inputNext + ".*' Return type, kejijingsai"
            print(sql)
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})


# 文件
@csrf_exempt
def wenjian_findTypes(request):
    global graph
    sql = "MATCH (n:WenJianMingCheng) RETURN n as type"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def wenjian_findByType(request):
    global graph
    type = eval(request.body.decode()).get('type')
    sql = "MATCH (n:WenJianMingCheng) -[]-> (wenjian:WenJian)  where n.`文件名称` = '"+ type +"' RETURN n as type, wenjian"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def wenjian_findByParams(request):
    global graph
    typePre = eval(request.body.decode()).get('typePre')
    typeNext = eval(request.body.decode()).get('typeNext')
    inputNext = eval(request.body.decode()).get('inputNext')

    if typePre == '' or typeNext == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            sql = "MATCH (type:WenJianMingCheng) -[]-> (wenjian:WenJian) where type.`文件名称` = '"+typePre+"' and wenjian.`"+typeNext+"` =~'.*" + inputNext + ".*' Return type, wenjian"
            print(sql)
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})


# BOM
@csrf_exempt
def bom_findTypes(request):
    global graph
    sql = "MATCH (n:BOM) where n.Pos =~ '.*1.*' RETURN n as type"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def bom_findByType(request):
    global graph
    type = eval(request.body.decode()).get('type')
    # sql = "MATCH (n:WenJianMingCheng) -[]-> (wenjian:WenJian)  where n.`文件名称` = '"+ type +"' RETURN n as type, wenjian"
    sql = "MATCH (n:BOM) where n.Pos =~ '.*"+type+".*' RETURN n as bom"
    try:
        nodes_source = graph.run(sql).data()
        return JsonResponse({"status": "ok", "success": True, "data": nodes_source})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "ok", "success": False, "data": ex})

@csrf_exempt
def bom_findByParams(request):
    global graph
    typePre = eval(request.body.decode()).get('typePre')
    typeNext = eval(request.body.decode()).get('typeNext')
    inputNext = eval(request.body.decode()).get('inputNext')

    if typePre == '' or typeNext == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            sql = "MATCH (type:WenJianMingCheng) -[]-> (wenjian:WenJian) where type.`文件名称` = '"+typePre+"' and wenjian.`"+typeNext+"` =~'.*" + inputNext + ".*' Return type, wenjian"
            print(sql)
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})


@csrf_exempt
def findBySql(request):
    global graph
    sql = eval(request.body.decode()).get('sql')
    if sql == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})