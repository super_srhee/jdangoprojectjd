from django.conf.urls import url
from yanjiusheng.views import *

urlpatterns = [
    url('^lunwen/findTypes',lunwen_findTypes),
    url('^lunwen/findByType', lunwen_findByType),
    url('^lunwen/findByParams', lunwen_findByParams),

    url('^kejijingsai/findTypes',kejijingsai_findTypes),
    url('^kejijingsai/findByType', kejijingsai_findByType),
    url('^kejijingsai/findByParams', kejijingsai_findByParams),

    url('^wenjian/findTypes', wenjian_findTypes),
    url('^wenjian/findByType', wenjian_findByType),
    url('^wenjian/findByParams', wenjian_findByParams),

    url('^bom/findTypes', bom_findTypes),
    url('^bom/findByType', bom_findByType),
    url('^bom/findByParams', bom_findByParams),

    url('^findBySql', findBySql),
]