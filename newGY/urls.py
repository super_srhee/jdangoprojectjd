from django.conf.urls import url
from newGY.views import *

urlpatterns = [
    url('^jiansuo/findTypes',gy_findTypes),
    url('^jiansuo/findByType', gy_findByType),
    url('^jiansuo/findByParams', gy_findByParams),

    url('^findBySql', findBySql),
]