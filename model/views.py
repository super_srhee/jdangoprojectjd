from django.shortcuts import render
import json,re,random,io,sys,requests,time

from django.views import View
from django.http import JsonResponse,QueryDict,HttpResponse
from django.views.decorators.csrf import csrf_exempt

from py2neo import Graph,Node,Relationship
graph = Graph(host='localhost',auth=('neo4j','123'))

# Create your views here.
@csrf_exempt
def findKeCheng(request):
    global graph
    # sql = eval(request.body.decode()).get('sql')
    typePre = eval(request.body.decode()).get('typePre')
    inputPre = eval(request.body.decode()).get('inputPre')
    typeRel = eval(request.body.decode()).get('typeRel')
    typeNext = eval(request.body.decode()).get('typeNext')
    inputNext = eval(request.body.decode()).get('inputNext')

    if typePre == '' or typeNext == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            sql = "match (a:"+typePre+") -[b:"+typeRel+"] ->(c:"+typeNext+") where a.name  =~'.*" + inputPre + ".*' and c.name  =~'.*" + inputNext + ".*'  return a,b,c"
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})

@csrf_exempt
def findBySql(request):
    global graph
    sql = eval(request.body.decode()).get('sql')
    if sql == '':
        return JsonResponse({"status":"error","success":False})
    else:
        try:
            nodes_source = graph.run(sql).data()
            return JsonResponse({"status": "ok","success": True,"data":nodes_source})
        except Exception as ex:
            print(ex)
            return JsonResponse({"status": "ok","success": False,"data":ex})


@csrf_exempt
def fetchSearch(request):
    global graph
    input1 = eval(request.body.decode()).get('input1')
    input2 = eval(request.body.decode()).get('input2')
    selectValue = eval(request.body.decode()).get('selectValue')
    print(input1,input2,selectValue)

    if selectValue == "topic-meta":
        sql = "match(n:topic)-[:HAS]->(m:meta) where n.info =~'.*"+input1+".*' and m.info =~'.*"+input2+".*' return n,m"
        nodes_source = graph.run(sql).data()
    if selectValue == "meta-content":
        sql = "match(n:meta)-[:HAS]->(m:content) where n.info =~'.*" + input1 + ".*' and m.info =~'.*" + input2 + ".*' return n,m"
        nodes_source = graph.run(sql).data()
    if selectValue == "content-合金成分及热处理制度":
        sql = "match(n:content)-[:HAS]->(m:合金成分及热处理制度) where n.info =~'.*"+input1+".*' and m.info =~'.*"+input2+".*' return n,m"
        nodes_source = graph.run(sql).data()
    if selectValue == "content-性能1":
        sql = "match(n:content)-[:HAS]->(m:性能1) where n.info =~'.*"+input1+".*' and m.info =~'.*"+input2+".*' return n,m"
        nodes_source = graph.run(sql).data()
    if selectValue == "content-性能2":
        sql = "match(n:content)-[:HAS]->(m:性能2) where n.info =~'.*"+input1+".*' and m.info =~'.*"+input2+".*' return n,m"
        nodes_source = graph.run(sql).data()

    return JsonResponse({"status": "ok","success":True,"data":nodes_source})