from flask import Flask,request,jsonify
import csv,time,requests,json
from py2neo import Graph,Node,Relationship,NodeMatch


# 执行写入数据
g = Graph(host='localhost',auth=('neo4j','123'))

g.run('match(n) detach delete n')
# test_node_1=Node("图谱名称",name="现代设计方法")
# g.create(test_node_1)
# test_node_1['内容']='以《现代设计方法》为框架搭建现代设计方法知识图谱'
# g.push(test_node_1)



with open('./json/Co总-副本.json', 'r', encoding='utf-8') as f:
    ret_dic = json.load(f)
    print(type(ret_dic)) # 结果 <class 'dict'>
    print(ret_dic['data'])
    reader = ret_dic['data']
    root_node = Node("topic",name='Co总',info='Co总')
    count = 0;# 计数
    for item in reader:
        # print("当前内容：", item)
        count = count+1
        print(count)
        meta = item['meta']
        content = item['content']
        print(meta)
        meta_node=Node("meta",name=str(count)+meta['标题'],标题=meta['标题'],材料分类=meta['材料分类'],DOI=['DOI'],数据摘要=meta['数据摘要'],
                       关键词=meta['关键词'],提交者=meta['提交者'],创建时间=meta['创建时间'],来源=meta['来源'],
                       引用=meta['引用'],审核人=meta['审核人'],Reviewers_Institution=meta["Reviewer's Institution"],其他信息=meta['其他信息'],info=meta['标题']+meta['数据摘要']+meta['提交者']+meta['审核人'])
        content_node = Node("content",name='Co总',info='Co总')
        rel_root_meta = Relationship(root_node, "HAS", meta_node)
        rel_root_content = Relationship(root_node, "HAS", content_node)
        g.create(rel_root_meta)
        g.create(rel_root_content)


        #合金成分及热处理制度
        hjcf = item['content']['合金成分及热处理制度']
        count1 = 0;
        for item_c in hjcf:
            count1 = count1+1
            hjcf_node = Node("合金成分及热处理制度", name=str(count1)+"合金成分及热处理制度",合金名称=item_c['合金名称（at %）'],固溶温度1=item_c["固溶温度1-1（℃）"],
                             固溶时间1=item_c["固溶时间1-1（h）"],固溶温度2=item_c["固溶温度1-2（℃）"],固溶时间2=item_c["固溶时间1-2（h）"],
                             时效温度=item_c["时效温度（℃）"],时效时间=item_c["时效时间（h）"],info=item_c['合金名称（at %）']+item_c["固溶温度1-1（℃）"])
            rel_content_hjcf = Relationship(content_node, "HAS", hjcf_node)
            g.create(rel_content_hjcf)

        # 性能1
        xingneng1 = item['content']['性能1']
        count2 = 0;
        for item_x1 in xingneng1:
            count2 = count2 + 1
            x1_node = Node("性能1",name=str(count2)+"性能1",相溶解温度=item_x1["γ'相溶解温度（℃）"],固相线温度=item_x1["固相线温度（℃）"],
                           液相线温度=item_x1["液相线温度（℃）"],密度=item_x1["密度（g/cm3）"],相体积分数=item_x1["γ'相体积分数（%）"],
                           Vickers_microhardness=item_x1["Vickers microhardness（Gpa）"],洛氏硬度=item_x1["洛氏硬度（HRC）"],维氏硬度=item_x1["维氏硬度（Hv）"]
                           ,info=item_x1["γ'相溶解温度（℃）"]+item_x1["密度（g/cm3）"])
            rel_content_x1 = Relationship(content_node, "HAS", x1_node)
            g.create(rel_content_x1)
        # 性能2
        xingneng2 = item['content']['性能2']
        count3 = 0
        for item_x2 in xingneng2:
            count3 = count3 + 1
            x2_node = Node("性能2",name=str(count3)+"性能2",相尺寸=item_x2["γ'相尺寸（nm）"],Lattice_misfit=item_x2["Lattice misfit（%）"],Tensile_test=item_x2["Tensile test"],
                           Elongation_at_break=item_x2["Elongation at break（%）"],area_fractions_Af=item_x2["γ'area fractions Af（%）"],
                           Mean_oxide_layer_thickness=item_x2["Mean oxide layer thickness"],info=item_x2["γ'相尺寸（nm）"])
            rel_content_x2 = Relationship(content_node, "HAS", x2_node)
            g.create(rel_content_x2)

# g.merge(test_node_1,"图谱名称","name")