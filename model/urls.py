from django.conf.urls import url
from model.views import *

urlpatterns = [
    url('^findByParams',findKeCheng),
    url('^findBySql', findBySql),
    url('^fetchSearch', fetchSearch),
]