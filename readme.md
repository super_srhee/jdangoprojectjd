
**新建模块**
python manage.py startapp xxx

**生成requirements.txt**
第一种：pipreqs . --encoding=utf8 --force
第二种：pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt

**常用镜像**
清华：https://pypi.tuna.tsinghua.edu.cn/simple
阿里云：http://mirrors.aliyun.com/pypi/simple/
中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/
华中理工大学：http://pypi.hustunique.com/
山东理工大学：http://pypi.sdutlinux.org/ 
豆瓣：http://pypi.douban.com/simple/

**安装 requirements.txt**
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt

**启动**
python manage.py runserver 0.0.0.0:9000

# model迁移
python manage.py makemigrations
python manage.py migrate
当出现duplicate column name: ID 存在相同id，去到migrations 下删掉队对应记录

# 问题
python 3.10  对应pandas 版本1.4.0
 