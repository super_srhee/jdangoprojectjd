from django.conf.urls import url
from kecheng.views import *

urlpatterns = [
    url('^findByParams',findKeCheng),
    url('^findBySql', findBySql),

    url('^pois_getAll', pois_getAll)
]