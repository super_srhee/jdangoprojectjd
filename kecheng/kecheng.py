from flask import Flask,request,jsonify
import csv,time,requests
from py2neo import Graph,Node,Relationship,NodeMatch
import py2neo

# 执行写入数据
g = Graph(host='localhost',auth=('neo4j','123'))

g.run('match(n)detach delete n')
test_node_1=Node("图谱名称",name="现代设计方法")
g.create(test_node_1)
test_node_1['内容']='以《现代设计方法》为框架搭建现代设计方法知识图谱'
g.push(test_node_1)


with open('CSV/先后.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("知识点",name=item[1])
        relation=Relationship(end_node,item[2],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "知识点", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/2.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("方法学",name=item[0])
        end_node=Node("知识点",name=item[1])
        relation=Relationship(end_node,item[2],start_node)
        g.merge(start_node, "方法学", "name")
        g.merge(end_node, "知识点", "name")
        g.merge(relation, "方法学", "name")


with open('CSV/3.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("方法",name=item[1])
        relation=Relationship(end_node,item[2],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "方法", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/类.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("类",name=item[0])
        end_node=Node("图谱名称",name=item[1])
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "类", "name")
        g.merge(end_node, "图谱名称", "name")
        g.merge(relation, "类", "name")

with open('CSV/设计.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("设计",name=item[0])
        end_node=Node("类",name=item[1])
        start_node['内容'] = item[2]
        relation=Relationship(end_node,item[4],start_node)
        g.merge(start_node, "设计", "name")
        g.merge(end_node, "类", "name")
        g.merge(relation, "设计", "name")

with open('CSV/设计-知识点.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("设计",name=item[1])
        start_node['内容'] = item[2]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "设计", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/设计方法学.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("知识点",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "知识点", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/方法学-知识点.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("方法学",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "方法学", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/方法学.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("方法学",name=item[0])
        end_node=Node("类",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "方法学", "name")
        g.merge(end_node, "类", "name")
        g.merge(relation, "方法学", "name")

with open('CSV/设计方法.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("知识点",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        start_node['图片'] = item[5]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "知识点", "name")
        g.merge(end_node, "知识点", "name")
        g.merge(relation, "知识点", "name")

with open('CSV/方法.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("方法",name=item[0])
        end_node=Node("类",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        start_node['图片'] = item[5]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "方法","name")
        g.merge(end_node, "类", "name")
        g.merge(relation, "方法", "name")

with open('CSV/方法-知识点.csv','r',encoding='gbk')as f:
    reader=csv.reader(f)
    for item in reader:
        if reader.line_num==1:
            continue
        print("当前行数", reader.line_num, "当前内容：", item)
        start_node=Node("知识点",name=item[0])
        end_node=Node("方法",name=item[1])
        start_node['内容'] = item[2]
        start_node['补充内容'] = item[4]
        relation=Relationship(end_node,item[3],start_node)
        g.merge(start_node, "知识点","name")
        g.merge(end_node, "方法", "name")
        g.merge(relation, "知识点", "name")

g.merge(test_node_1,"图谱名称","name")