from flask import Flask,request,jsonify
import csv,time,requests,json
from py2neo import Graph,Node,Relationship,NodeMatch


# 执行写入数据
g = Graph(host='localhost',auth=('neo4j','123'))

g.run('match(n) detach delete n')

with open('./topics1.csv', 'r', encoding='utf-8') as f:
    reader = csv.reader(f)
    count = 0
    paper_node = Node("paper", name='paper')
    for item in reader:
        print(item)
        count = count+1
        topic_node = Node("topics", name='topic'+str(count))
        rel_paper_topic = Relationship(topic_node, "Rel", paper_node)
        g.create(rel_paper_topic)
        for i in range(0, len(item)):
            keyword_node = Node("keywords",name=item[i])
            rel_topic_keyword = Relationship(keyword_node, "Rel", topic_node)
            g.create(rel_topic_keyword)
