from django.conf.urls import url
from user.views import *

urlpatterns = [
    # url('^api$',RestApi.as_view()),
    # url('^createNode',createNode),
    # url('^fetchNodes',fetchNodes),
    # url('^fetchEquipments',fetchEquipments),
    # url('^fetchStations',fetchStations),
    # url('fetchMPs',fetchMPs),
    url('register',registerUser),
    url('login',logIn)
]