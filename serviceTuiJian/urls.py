from django.conf.urls import url
from serviceTuiJian.views import *

urlpatterns = [
    url('^initServices',initServices),
    url('^getServerHostAndServiceAndLayerByUrl',getServerHostAndServiceAndLayerByUrl),
    url('fetchJieba',fetchJieba),
    url('fetchLayer',fetchLayer),
    url('fetchSearch',fetchSearch),
    url('jiebaSearch',fetchJiebaSearch),
    url('goLike',goLike),
    url('tuiJian',tuiJian),
]