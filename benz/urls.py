from django.conf.urls import url
from benz.views import *

urlpatterns = [
    url('^api$',RestApi.as_view()),
    url('^createNode',createNode),
    url('^fetchNodes',fetchNodes),
    url('^fetchEquipments',fetchEquipments),
    url('^fetchStations',fetchStations),
    url('fetchMPs',fetchMPs),
    url('fetchDEs',fetchDEs),
]