from django.db import models

# Create your models here.

class ImageItem(models.Model):
    name = models.TextField()
    alias = models.TextField()
    url = models.TextField()
    icon = models.ImageField(null=True,blank=True,upload_to="static/upload") # 上传到upload路径下
    extent = models.TextField(null=True);
    other = models.TextField()


    def __str__(self):
        return self.name


