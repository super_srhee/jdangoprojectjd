from django.shortcuts import render
from django.views import View
from django.http import JsonResponse,QueryDict,HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json,re,random,io,sys,requests,time,base64,os,uuid
from .models import ImageItem
from django.core import serializers
# Create your views here.

@csrf_exempt
def addImageItem(request):
    try:
        requestData = json.loads(request.body)
        if requestData is None:
            return JsonResponse({"status": "error", "success": False,'data':'请正确传入参数'})
        entity = ImageItem(
            name = requestData['name'],
            alias = requestData['alias'],
            url = requestData['url'],
            icon = requestData['icon'],
            other = requestData['other'])
        entity.save()
        return JsonResponse({"status": "ok", "success": False, "data": ''})
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "error", "success": False})

def getAll(request):
    try:
        all = serializers.serialize("json",ImageItem.objects.all())
        return JsonResponse({"status": "ok", "success": True, "data": all},safe=False)
    except Exception as ex:
        print(ex)
        return JsonResponse({"status": "error", "success": False})