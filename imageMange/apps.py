from django.apps import AppConfig


class ImagemangeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'imageMange'
