# Generated by Django 3.0.8 on 2022-02-17 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('imageMange', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imageitem',
            name='icon',
            field=models.ImageField(blank=True, null=True, upload_to='upload'),
        ),
    ]
