# Generated by Django 3.0.8 on 2022-02-17 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ImageItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('alias', models.TextField()),
                ('url', models.TextField()),
                ('icon', models.TextField()),
                ('other', models.TextField()),
            ],
        ),
    ]
