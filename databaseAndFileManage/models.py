from django.db import models

# Create your models here.

# 前端：【mapbox-diatomdaomap.com】
class MapboxImage(models.Model):

    countryCode = models.TextField()
    location = models.TextField()
    uuid = models.TextField()
    lng = models.FloatField()
    lat = models.FloatField()

    def __str__(self):
        return self.uuid
# end