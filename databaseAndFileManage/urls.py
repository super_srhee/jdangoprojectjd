from django.conf.urls import url
from databaseAndFileManage.views import *

urlpatterns = [
    url('^mapbox_addImage',mapbox_addImage),
    url('^mapbox_getAll', mapbox_getAll),
    url('^mapbox_deleteCard', mapbox_deleteCard),
]