from django.conf.urls import url
from country.views import *

urlpatterns = [
    url('^login',login),
    url('^initAll',initAll),
    url('^commonSearch',commonSearch),
    # url('^getServerHostAndServiceAndLayerByUrl',getServerHostAndServiceAndLayerByUrl),
    # url('fetchJieba',fetchJieba),
    # url('fetchLayer',fetchLayer),
    # url('fetchSearch',fetchSearch),
    # url('jiebaSearch',fetchJiebaSearch),
    # url('goLike',goLike),
    # url('tuiJian',tuiJian),
]